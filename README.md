# Среда разработки PHP на базе Docker

Публикация на **Habr**: https://habr.com/ru/post/519500/.

Основано на статье выше

## Установка

1. Копировать **.env-example** в **.env**, изменить нужные параметры (например, подключение к базе данных и APP_CODE_PATH_HOST)
2. ```docker-compose build```
3. ```docker-compose up -d```
